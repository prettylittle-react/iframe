import React from 'react';
import PropTypes from 'prop-types';

import './IFrame.scss';

/**
 * IFrame
 * @description [Description]
 * @example
  <div id="IFrame"></div>
  <script>
    ReactDOM.render(React.createElement(Components.IFrame, {
        title : 'Example IFrame'
    }), document.getElementById("IFrame"));
  </script>
 */
class IFrame extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'iframe';
	}

	render() {
		const {src, className} = this.props;

		return <iframe className={`${this.baseClass} ${className}`} src={src} />;
	}
}

IFrame.defaultProps = {
	src: '',
	className: ''
};

IFrame.propTypes = {
	src: PropTypes.string.isRequired,
	className: PropTypes.string
};

export default IFrame;
